#!/usr/bin/ruby -w

line_number = 0
title = ''

begin
    bible_txt = File.new('NIV_bible.txt', 'r')
    new_bible_txt = File.new('NIV_bible_new.txt', 'w+')

    while (line = bible_txt.gets)
        line.strip!
        if "#{line}".empty?
            line_number += 1
        else
            if line_number == 1
                title = line[1...-1]
                line_number = 0
            else
                new_bible_txt.puts "#{title}:#{line}"
            end
        end
    end

    new_bible_txt.close
    bible_txt.close
rescue => err
    puts "Exception: #{err}"
    err
end