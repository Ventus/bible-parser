# -*- coding:utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

bible_txt = open('ch_ot_sim.txt', 'r')
new_bible_txt = open('ch_ot_sim_new.txt', 'w')

keep_line = ""


def bible_parsing(line):
    global new_bible_txt, keep_line
    tmp = line.split(":")

    line = line.replace('\n', '')
    line = line.replace('\r\n', '')

    if len(tmp) > 1:
        new_bible_txt.write(keep_line + '\n')
        keep_line = line
    else:
        keep_line = keep_line+line


for line in bible_txt.xreadlines():
    if line != "":
        # pass
        bible_parsing(line)

bible_txt.close()
new_bible_txt.close()
