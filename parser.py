# -*- coding:utf-8 -*-
import optparse
import re
import sys
import os
reload(sys)
sys.setdefaultencoding('utf-8')

pattern_set = {'ko': '([\u3131-\u3163\uac00-\ud7a3]{1})', 'en': '([A-Za-z\s]*)', 'cn': '([^0-9\s]*)'}

preface_pattern_str = ''
preface_pattern = ''

chapter_pattern_str = '([0-9]*)'
chapter_pattern = re.compile(chapter_pattern_str)

preface = ""
preface_dir = ""
chapter = 0
chapter_file = ""
root_dir = ""
preface_number = 0


def set_lang_pattern(perface_name):
    global preface_pattern_str, preface_pattern, pattern_set
    preface_pattern_str = pattern_set[perface_name]
    preface_pattern = re.compile(preface_pattern_str)


def make_preface_dir(perface_name):
    global preface_dir, root_dir
    if int(perface_name) < 10:
        perface_name = '0' + str(perface_name)
    preface_dir = str(perface_name)
    os.mkdir(root_dir+'/'+preface_dir)


def open_chapter_file(chapter_name):
    global preface_dir, chapter_file, root_dir, preface
    if int(chapter_name) < 10:
        chapter_name = '0' + str(chapter_name)
    file_name = './'+root_dir+'/'+preface_dir + '/'+chapter_name+'.html'

    chapter_file = open(file_name, 'w')
    chapter_file.writelines('<!DOCTYPE HTML>\n')
    chapter_file.writelines('<html>\n')
    chapter_file.writelines('<head>\n')
    chapter_file.writelines('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n')
    chapter_file.writelines('<title>' + preface + chapter_name + '</title>\n')
    chapter_file.writelines('</head>\n')
    chapter_file.writelines('<body>\n')


def close_chapter_file():
    global chapter_file
    chapter_file.writelines('</body>\n')
    chapter_file.writelines('</html>\n')
    chapter_file.close()


def bible_parsing(line, type='ko'):
    global preface_dir, chapter_file, chapter, preface, preface_number
    tmp = line.split(":")

    if len(tmp) < 2:
        return 0

    tmp = tmp[0]

    if type == 'ko':
        preface_str = re.split(preface_pattern, tmp)[0]
        chapter_str = re.split(chapter_pattern, tmp)[1]
    elif type == 'cn':
        preface_str = re.split(preface_pattern, tmp)[1]
        chapter_str = re.split(preface_pattern, tmp)[2]
    else:
        divide = re.split(preface_pattern, tmp)
        preface_str = divide[0] + divide[1]
        chapter_str = re.split(chapter_pattern, divide[2])[1]

    preface_str = preface_str.strip()
    chapter_str = chapter_str.strip()

    # 서가 바뀌는 부분 체크
    if preface != preface_str:
        chapter = 0
        preface = preface_str
        preface_number = preface_number + 1
        make_preface_dir(preface_number)

    # 장이 바뀌는 부분 체크
    if chapter != chapter_str:
        chapter = chapter_str
        if chapter_file != "":
            # 장이 바뀌면 장 닫기
            close_chapter_file()
        open_chapter_file(chapter_str)

    write_bible(line)


def write_bible(line):
    global chapter_file
    if line[len(line)-1] == '\n':
        line = line[:len(line)-1]
    html_line = '<p>'+line+'<br/></p>\n'
    chapter_file.writelines(html_line)


def main():
    global preface_dir, chapter_file, chapter, preface, root_dir

    parser = optparse.OptionParser('usage : python parser -F <bible file> -L <language types : ko or en or cn>')
    parser.add_option('-F', dest='bibleFile', type='string', help='specify bible file')
    parser.add_option('-L', dest='langType', type='string', help='specify language types')

    (option, args) = parser.parse_args()
    bibleFile = option.bibleFile
    langType = option.langType

    if bibleFile is None or langType is None:
        print parser.usage
        exit(0)

    bible = open(bibleFile, 'r')

    # 언어 별 패턴 생성
    set_lang_pattern(langType)

    # 파일 이름으로 디렉토리 만들기
    root_dir = bibleFile.split('.')[0]
    os.mkdir(root_dir)

    for line in bible.xreadlines():
        # 빈 공백 라인 체크
        if line != "" and line != "\n" and line != "\r\n":
            # pass
            bible_parsing(line, langType)

    bible.close()

if __name__ == '__main__':
    main()
