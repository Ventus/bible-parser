# -*- coding:utf-8 -*-
import sys
import re
reload(sys)
sys.setdefaultencoding('utf-8')

preface_pattern_str = '([^0-9\s]*)'
preface_pattern = re.compile(preface_pattern_str)

chapter_pattern_str = '([0-9]*)'
chapter_pattern = re.compile(chapter_pattern_str)

bible_txt = open('ch_ot_sim_new.txt', 'r')
test = open('test.txt', 'w')


def bible_parsing(line):
    global test, keep_line
    tmp = line.split(":")

    # print tmp
    # print re.split(preface_pattern, tmp)
    tmp = tmp[0]
    test.writelines(line)
    print re.split(preface_pattern, tmp)[1]
    print re.split(preface_pattern, tmp)[2]


for line in bible_txt.xreadlines():
    if line != "" and line != "\n" and line != "\r\n":
        # pass
        bible_parsing(line)

bible_txt.close()
test.close()
